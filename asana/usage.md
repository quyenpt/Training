# Asana

Cùng với Trello, Redmine hay Toodledo, Basecamp, Clinked, phần mềm Asana cũng là một công cụ khá thú vị và tiện ích giúp người sử dụng có thể làm việc theo nhóm, quản lý công việc mà không cần thông qua email.
Đây là giao diện chính của Asana

![Main](images/main_ui.png)

Có thể lựa chọn nhóm làm việc ở phần `Teams` ở pane phía bên trái. Trước khi làm việc với projec nào, cần lựa chọn team trước. Hoặc có thể truy cập nhanh tới project bằng cách ấn vào ở đầu tên team.
Xem các thông báo ở phần `Inbox`, các task được gắn cho bạn ở phần `My Task`.

### Cách tạo project trong Asana:

- Click vào biểu tượng ![+](images/add_project_btn.jpeg) (ở góc trên bên phải) hoặc (+ -> Projects) ở phía cuối tên team của pane bên trái.
- Một project mới sẽ được tạo ra cùng với một không gian làm việc ở pane chính giữa.
- Sau đó, bạn có thể tùy ý đặt tên cho Project và thêm vào những task cần làm.

### Cách tạo task trong Asana:
- Thêm Section: Click vào một dòng, gõ tên section và kết thúc bằng ":" hoặc ấn vào nút <Add Section>. Một Section bao gồm nhiều task
- Thêm Task: Click vào một dòng, gõ tên task hoặc ấn vào nút <Add Task>
![add task](images/add_task_1.png)

![add task 2](images/add_task_2.png)

- Sau khi tạo task, gắn người thực hiện task đó thông qua nút <Assign> , deadline đặt ở mục <Due Date>. Bạn có thể tạo các task con trong task lớn bằng cách click vào nút Subtask.
 
![add task 3](images/add_task_3.png)
